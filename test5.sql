--「item」テーブルから「category_id」が『１』の列のみ
--取得するカラムは「item_name」「item_price」
SELECT
 item_name,
 item_price
 FROM
 item
 WHERE
 category_id = 1;