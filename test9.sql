--itemテーブルとitem_categoryテーブルを結合
--テーブルを「category_id」で集計
--取得するカラムは「category_name」「item_priceの合計→total_price」
--total_priceを値が大きい順に並べる

SELECT
 t.category_name,
 SUM(t1.item_price) AS total_price
 FROM
 item_category t
 INNER JOIN
 item t1
 ON
 t.category_id = t1.category_id
 GROUP BY
 t.category_name
  ORDER BY
 total_price DESC;