--「item」テーブルから「item_price」が1000以上
--取得カラムは「item_name」と「item_price」

SELECT
 item_name,
 item_price
 FROM
 item
 WHERE
 item_price >= 1000;