--「item」テーブルから「item_name」に「肉」を含む列を抽出
--取得するカラムは「item_name」「item_price」

SELECT
 item_name,
 item_price
 FROM
 item
 WHERE
 item_name LIKE '%肉%';