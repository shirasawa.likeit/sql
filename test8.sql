--itemテーブルとitem_categoryテーブルを結合
--取得するカラムは「item_id」「item_name」「item_price」「category_name」

SELECT
 t.item_id,
 t.item_name,
 t.item_price,
 t1.category_name
 FROM
 item t
 INNER JOIN
 item_category t1
 ON
 t.category_id = t1.category_id;